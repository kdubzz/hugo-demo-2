---
title: "Creating a static website with Hugo - What I Learned (part 1)"
date: 2020-06-13T14:23:17-07:00
draft: false
---
Building a simple static website with Hugo/Go!

Things I've learned so far

1. **Hugo** _commands_
  - `hugo new site <site_name>` will generate a new static site with the name specified in `<site_name>`.  
  - `hugo new <file_name>.md` will generate a new file, which will be placed in the `./contents/` directory
      - Of course, creating a new file can be done manually within your project folder.

2. **Hugo** project _file structure_
  - Each Hugo Project is composed of the following directory structure
  ```
 ./<root_dir>
    archetypes/
    content/
    data/
    layouts/
    resources/
    static/
    themes/
    config.toml
  ```

3. **Hugo** _partials_


...to be continued
